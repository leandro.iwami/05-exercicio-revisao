package br.com.itau.investimento.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.services.CatalogoService;
import br.com.itau.investimento.services.SimulacaoService;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

	@Autowired
	SimulacaoService simulacaoService;
	
	@PostMapping("/{idCliente}/simular")
	public List<Simulacao> simular( @PathVariable int idCliente, @RequestBody Aplicacao aplicacao){
		return simulacaoService.calcular(aplicacao, idCliente);
	}
}
