package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.ClienteSaida;

import br.com.itau.investimento.repositories.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
	ClienteRepository clienteRepository;
	
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public ClienteSaida obterClientePorIdSaida(int id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);
		Cliente cliente = clienteOptional.get();
		ClienteSaida clientesaida = new ClienteSaida();
		clientesaida.setCpf(cliente.getCpf());
		clientesaida.setNome(cliente.getNome());
		return clientesaida;
	}
	
	Cliente obterClientePorId(int id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);
		Cliente cliente = new Cliente(); 
		cliente = clienteOptional.get();	
		return cliente;
	}

	public void inserir(Cliente cliente) {
		String hash = encoder.encode(cliente.getSenha());
		cliente.setSenha(hash);

		clienteRepository.save(cliente);
	}

	public boolean atualizar(Cliente cliente, int id) {
		cliente.setId(id);
		Optional<Cliente> clienteOptional = clienteRepository.findById(cliente.getId());

		if (clienteOptional.isPresent()) {
			cliente = mesclarAtributos(cliente, clienteOptional.get());
			clienteRepository.save(cliente);
			return true;
		}
		return false;
	}

	private Cliente mesclarAtributos(Cliente novo, Cliente antigo) {
		
		if (novo.getNome() != null && !novo.getNome().isEmpty()) {
			antigo.setNome(novo.getNome());
		}
				
		if (novo.getCpf() != 0) {
			antigo.setCpf(novo.getCpf());
		}
		

		return antigo;
	}

}
