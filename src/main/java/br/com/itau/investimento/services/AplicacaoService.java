package br.com.itau.investimento.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.AplicacaoSaida;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.repositories.AplicacaoRepository;

@Service
public class AplicacaoService {
	
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	CatalogoService catalogoService;
	
	
	public Iterable<AplicacaoSaida> listarAplicacoes(int id){
		
		List<AplicacaoSaida> listasaida = new ArrayList<AplicacaoSaida>();
		
		Cliente cliente = clienteService.obterClientePorId(id);

		Iterable<Aplicacao> listaaplicacao = aplicacaoRepository.findAllByCliente(cliente);
		
		for (Aplicacao aplicacao : listaaplicacao) {
			AplicacaoSaida aplicacaoSaida = new AplicacaoSaida();
			aplicacaoSaida.setIdAplicacao(aplicacao.getIdAplicacao());
			aplicacaoSaida.setMes(aplicacao.getMeses());
			Produto produto = aplicacao.getProduto();
			Optional<Produto> produtoOptional = catalogoService.obterProdutoPorId(produto.getIdProduto());
			aplicacaoSaida.setProduto(produtoOptional.get());
			aplicacaoSaida.setValor(aplicacao.getValor());
			listasaida.add(aplicacaoSaida);
			
		}
		return listasaida;
	}
	
}
