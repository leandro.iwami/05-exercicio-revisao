package br.com.itau.investimento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimento.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
