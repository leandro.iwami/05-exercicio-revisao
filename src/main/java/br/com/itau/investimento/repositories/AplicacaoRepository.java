package br.com.itau.investimento.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Cliente;

public interface AplicacaoRepository extends JpaRepository<Aplicacao, Integer> {

	public Iterable<Aplicacao> findAllByCliente(Cliente cliente);
}
